# `xstream-upon-stop`

```
pnpm install --save xstream-upon-stop
```

An xstream operator to run an imperative function upon the lifecycle-stop of the stream

## usage

```js
import fromDiagram from 'xstream/extra/fromDiagram'
import uponStop from 'xstream-upon-stop'

let is_complete = false
const stream = fromDiagram('1----2--3--4----5|')
  .compose(uponStop(() => {
    console.log('stopping')
  }))

stream.addListener({
  next: i => console.log(i),
  error: err => console.error(err),
  complete: () => console.log('completed')
})
```

```text
> 1  (after 60 ms)
> 2  (after 160 ms)
> 3  (after 220 ms)
> 4  (after 280 ms)
> 5  (after 380 ms)
> completed
> stopping
```

## License

MIT

