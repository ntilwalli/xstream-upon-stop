/// <reference types="mocha"/>
/// <reference types="node" />
import xs, {Stream} from 'xstream';
import uponStop from './index';
import * as assert from 'assert';

describe('sample', () => {
  it('should call function uponStop', (done: any) => {
    let check = 0
    const stream1 = xs
      .periodic(100)
      .take(3)
    const stream = stream1.compose(uponStop(() => {check++}));
    stream.addListener({
      next: (x: number) => {
        check++
      },
      error: done,
      complete: () => {
        assert.equal(check, 4);
        done();
      },
    });
  });
});
