function noop() {}

module.exports = {
  require: {
    'xstream/extra/fromDiagram': require('xstream/extra/fromDiagram').default,
    ['xstream-upon-stop']: require('./index').default
  },

  globals: {
    setInterval: noop,
    console: {
      log: noop,
      error: noop,
    },
    listener: {
      next: noop,
      error: noop,
      complete: noop,
    },
  },
};
